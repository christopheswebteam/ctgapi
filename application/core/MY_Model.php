<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Model extends CI_Model{

	function set_post($key, $value)
	{
		$_POST[$key] = $value;
	}

	function post_table_data($table, $forge = FALSE)
	{
		$db_data = array( );

		foreach ($this->db->list_fields($table) as $field) {

			if ($this->input->post($field) !== FALSE) {

				$db_data[$field] = $this->input->post($field);
				
			} elseif ($forge) {

				$db_data[$field] = $this->input->post($field);

				$fields[trim($field)]['type'] = "VARCHAR";
				$fields[trim($field)]['constraint'] = "255";
				
			}
			
		}

		if (isset($fields)) {

			$this->dbforge->add_column($table, $fields);
			
		}

		return $db_data;

	}
	
}