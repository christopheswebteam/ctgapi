<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'libraries/REST_Controller.php');

/**
 * @property Twitter $twitter
 * 
 * Models
 * 
 * @property Twitter_model $twitter_model
 */
class MY_Controller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

	}

}