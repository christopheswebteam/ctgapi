<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment_profiles_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_payment_profile_by_id($id)
    {
        $this->db->select();
        $this->db->from('payment_profiles');
        $this->db->where('id', $id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->row_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function get_payment_profile_by_authorizenet_payment_profile_id($authorizenet_payment_profile_id)
    {
        $this->db->select();
        $this->db->from('payment_profiles');
        $this->db->where('authorizenet_payment_profile_id', $authorizenet_payment_profile_id);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            $result = $query->row_array();
        } else {

            $result = FALSE;
        }

        return $result;
    }

    public function upsert_credit_card($customers_id, $payment_profiles_data)
    {
        try {
		
            $authorizenet_payment_profile_id = $this->_authorizenet_payment_profile($customers_id, $payment_profiles_data);
			
        } catch (Exception $ex) {
		
            $this->db->select();
            $this->db->from('payment_profiles');
            $this->db->where('customers_id', $customers_id);
            $this->db->where('cc_last_four', substr($payment_profiles_data['cc_number'], -4));
			
            $result = $this->db->get();
			
            if ($result->num_rows() > 0) {
			
                $row = $result->row_array();
				
                return $row['id'];
				
            } else {

                return FALSE;
            }
        }


        if ($authorizenet_payment_profile_id) {

            $payment_profiles_data['default'] = 1;
            $payment_profiles_data['type'] = "Credit Card";
            $payment_profiles_data['customers_id'] = $customers_id;
            $payment_profiles_data['date_added'] = date('Y-m-d H:i:s');
            $payment_profiles_data['authorizenet_payment_profile_id'] = $authorizenet_payment_profile_id;

            /*
             * Get the existing payment profile for the returned authorize.net payment profile id
             * so that if its a duplicate, we only update the data for it, otherwise we update the
             * payment profile.
             */
            $payment_profile = $this->get_payment_profile_by_authorizenet_payment_profile_id($authorizenet_payment_profile_id);

            $payment_profiles_data['cc_last_four'] = substr($payment_profiles_data['cc_number'], -4);

            unset($payment_profiles_data['cc_number']);
            unset($payment_profiles_data['cc_cvn']);

            if ($payment_profile) {

                $this->db->where('id', $payment_profile['id']);
                $this->db->update('payment_profiles', $payment_profiles_data);

                $payment_profiles_id = $payment_profile['id'];
				
            } else {

                $this->db->insert('payment_profiles', $payment_profiles_data);

                $payment_profiles_id = $this->db->insert_id();
				
            }

            return $payment_profiles_id;
			
        } else {

            return FALSE;
			
        }
    }

    public function _authorizenet_payment_profile($customers_id, $payment_methods_data)
    {
        //$this->load->library('authorizenet', array('98aD6W6sW', '5B92dHs2B57uX22G', FALSE));
        $this->load->library('authorizenet', array('98aD6W6sW', '444VaPc4WYY3m8Kn', FALSE));
        $this->load->model('v1/customers_model');

        $customer = $this->customers_model->get_customer_by_id($customers_id);

        /*
         * For the payment profile to be created, our system should have customer record available
         * and the authorize.net customer id should also be available.
         */
        if (($customer) && ($customer['authorizenet_cim_id'] !== 0)) {

            /*
             * Each credit card request is sent to authorize.net in order to make sure that card is 
             * authorized and is useable. 
             * 
             * For this we are also assuming that authorize.net will return the same payment profile id
             * for duplicate requests.
             */
            $this->authorizenet->createCustomerPaymentProfileRequest(array(
                'customerProfileId' => $customer['authorizenet_cim_id'],
                'paymentProfile' => array(
                    'billTo' => array(
                        'firstName' => $payment_methods_data['cc_name_first'],
                        'lastName' => $payment_methods_data['cc_name_last'],
                        'company' => '',
                        'address' => trim($payment_methods_data['cc_street_address_1'] . ' ' . $payment_methods_data['cc_street_address_2']),
                        'city' => $payment_methods_data['cc_city'],
                        'state' => $payment_methods_data['cc_state'],
                        'zip' => $payment_methods_data['cc_zip_code'],
                        'country' => 'USA',
                        'phoneNumber' => $customer['phone_number'],
                        'faxNumber' => ''
                    ),
                    'payment' => array(
                        'creditCard' => array(
                            'cardNumber' => $payment_methods_data['cc_number'],
                            'expirationDate' => $payment_methods_data['cc_exp_year'] . '-' . $payment_methods_data['cc_exp_month']
                        )
                    )
                ),
                'validationMode' => 'liveMode'
                //'validationMode' => 'testMode'
            ));

            if ($this->authorizenet->isSuccessful()) {

                return (int)$this->authorizenet->response_xml->customerPaymentProfileId;
				
            } else {
			
                if ($this->authorizenet->response_xml->messages->message->code . '' == 'E00039') {
				
                    throw new Exception("Duplicate payment profile exists.");
					
                }
				
                //TODO: Raise an exception here with the proper message returned by Authorize.net.
                // Need to find a way to lookup existing payment profiles.
                // Maybe we delete all payment profiles for customer if a duplicate is found then try again.
                return FALSE;
				
            }
			
        } else {

            //TODO: Raise an exception here to provide the information that customer profile was not found. 
            return FALSE;
			
        }
		
    }

}
