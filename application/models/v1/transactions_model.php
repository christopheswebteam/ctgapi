<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Transactions_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function process_order_transaction($orders_id, $payment_profiles_id, $cc_cvn)
    {
        //$this->load->library('authorizenet', array('98aD6W6sW', '5B92dHs2B57uX22G', FALSE));
        $this->load->library('authorizenet', array('98aD6W6sW', '444VaPc4WYY3m8Kn', FALSE));

        $this->load->model('v1/orders_model');
        $this->load->model('v1/payment_profiles_model');

        $order = $this->orders_model->get_order_by_id($orders_id);

        if ($order) {

            $payment_profile = $this->payment_profiles_model->get_payment_profile_by_id($payment_profiles_id);

            if ($payment_profile) {

                // Let's build array of line items.

                if (is_array($order['products'])) {

                   $line_items = array();

                    foreach ($order['products'] as $product) {

                        $line_item = array();

                        $line_item['itemId'] = $product['pos_sku'];
                        $line_item['name'] = substr($product['name'], 0, 30);
                        $line_item['description'] = substr($product['description'], 0, 254);
                        $line_item['quantity'] = $product['quantity'];
                        $line_item['unitPrice'] = $product['unit_price'];

                        $line_items[] = $line_item;

                    }
                    $this->authorizenet->createCustomerProfileTransactionRequest(array(
                        'transaction' => array(
                            'profileTransAuthCapture' => array(
                                'amount' => $order['total'], // changed from subtotal
                                'tax' => array(
                                    'amount' => $order['tax'],
                                    'name' => 'GA Tax',
                                    'description' => 'Georgia state sales tax.'
                                ),
                                'lineItems' => $line_items,
                                'customerProfileId' => $order['customer']['authorizenet_cim_id'],
                                'customerPaymentProfileId' => $payment_profile['authorizenet_payment_profile_id'],
                                'order' => array(
                                    'invoiceNumber' => $order['id'],
                                    'description' => 'Online order ID ' . $order['id'] . '.',
                                ),
                                'taxExempt' => 'false',
                                'recurringBilling' => 'false',
                                'cardCode' => $cc_cvn //'000'
                            )
                        )
                    ));

                    if ($this->authorizenet->isSuccessful()) {

						@mail('jaylogan@jaylogan.com', 'Succesful Authorize.net Transaction', print_r($this->authorizenet->response_xml, TRUE));

						// 1,1,1,This transaction has been approved.,110210,Y,6388958631,212,Online order ID 212.,17.49,CC,auth_capture,3,Jonathan,Logan,,2127 Longmont Dr.,Lawrenceville,GA,30044,USA,2407236556,,jaylogan@jaylogan.com,,,,,,,,,1.22,,,FALSE,,299AA57390068FE1BF4E1F63C22295C2,M,,,,,,,,,,,,XXXX0790,MasterCard,,,,,,,,,,,,,,,,
						$response = explode(',', $this->authorizenet->response_xml->directResponse);

						$transaction_data['orders_id'] = $order['id'];
						$transaction_data['payment_profiles_id'] = $payment_profiles_id;
						$transaction_data['gateway_message'] = (isset($response[3])) ? $response[3] : FALSE;
						$transaction_data['gateway_reference_id'] = (isset($response[4])) ? $response[4] : FALSE;
						$transaction_data['gateway_transaction_id'] = (isset($response[6])) ? $response[6] : FALSE;
						$transaction_data['total'] = (isset($response[9])) ? $response[9] : FALSE;
						$transaction_data['tax'] = (isset($response[32])) ? $response[32] : FALSE;
						$transaction_data['gateway_transaction_type'] = (isset($response[11])) ? $response[11] : FALSE;
						$transaction_data['gateway_response'] = serialize($response);
						$transaction_data['date_added'] = date('Y-m-d H:i:s');

						$this->db->insert('transactions', $transaction_data);

						$transactions_id = $this->db->insert_id();

						return $transactions_id;

                    } else {

						@mail('jaylogan@jaylogan.com', 'Declined Authorize.net Transaction', print_r($this->authorizenet->response_xml, TRUE));

                        return FALSE;

                    }

                } else {

                    return FALSE;

                }

            } else {

                return FALSE;

            }

        } else {

            return FALSE;

        }
    }


	public function get_transaction_by_order_id($order_id)
	{
		$this->db->select();
		$this->db->from('transactions');
		$this->db->where('orders_id', $order_id);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			return $result;

		} else {

			return FALSE;

		}

	}

}
