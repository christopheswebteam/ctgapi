<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Customers_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function get_customer_by_id($id)
    {
		$this->db->select();
		$this->db->from('customers');
		$this->db->where('id', $id);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			
		} else {

			$result = FALSE;

		}

        return $result;
    }

    public function update_customer_by_id($id, $db_data)
    {
        $this->db->where('id', $id);
        $this->db->update('customers', $db_data);
    }

    public function upsert_customer_by_email($customer_data)
    {
        if ((isset($customer_data['email_address'])) && (!empty($customer_data['email_address']))) {

            $this->db->select('id');
            $this->db->from('customers');
            $this->db->where('email_address', $customer_data['email_address']);

            $query = $this->db->get();

            if ($query->num_rows() > 0) {

                $result = $query->row_array();

                $customers_id = $result['id'];

                $this->update_customer_by_id($customers_id, $customer_data);

            } else {

                $customer_data['date_added'] = date('Y-m-d H:i:s');

                $this->db->insert('customers', $customer_data);

                $customers_id = $this->db->insert_id();

            }

            $customer_info = $this->get_customer_by_id($customers_id);

            if ($customer_info['authorizenet_cim_id'] == 0) {

                $authorizenet_cim_id = $this->_authorizenet_customer_profile($customers_id, $customer_info['email_address']);

                $this->update_customer_by_id($customers_id, array('authorizenet_cim_id' => (int)$authorizenet_cim_id));

            }

            return $customers_id;

        } else {

            return FALSE;

        }

    }

    public function _authorizenet_customer_profile($customers_id, $customers_email_address)
    {
        //$this->load->library('authorizenet', array('98aD6W6sW', '5B92dHs2B57uX22G', FALSE));
        $this->load->library('authorizenet', array('98aD6W6sW', '444VaPc4WYY3m8Kn', FALSE));

        $this->authorizenet->createCustomerProfileRequest(array(
            'profile'   =>  array(
                'merchantCustomerId'    =>  $customers_id,
                'email'                 =>  strtolower($customers_email_address)
            )
        ));

        if ($this->authorizenet->isSuccessful()) {

            return (int)$this->authorizenet->customerProfileId;

        } else {

            return FALSE;

        }

    }
}