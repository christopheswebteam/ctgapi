<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function insert_customer_order($customer_id, $order_data)
    {
		$datetime = date('Y-m-d H:i:s');
		
		$order_data['customers_id'] = $customer_id;
		
		if (!isset($order_data['date_added'])) {

			$order_data['date_added'] = $datetime;

		}
		
		if (!isset($order_data['date_last_modified'])) {

			$order_data['date_last_modified'] = $datetime;

		}

		$this->db->insert('orders', $order_data);

		return $this->db->insert_id();
    }
	
	public function update_customer_order($order_id, $customer_id, $order_data)
    {
		$order_data['customers_id'] = $customer_id;
		
		if (!isset($order_data['date_last_modified'])) {

			$order_data['date_last_modified'] = date('Y-m-d H:i:s');

		}
		
		$this->db->where('id', $order_id);
		$this->db->update('orders', $order_data);

		return $order_id;
    }
	
	public function clear_orders_products($orders_id)
    {
		$this->db->where('orders_id', $orders_id);
		$this->db->delete('orders_products');
    }

    public function insert_orders_product($order_id, $order_product)
    {
		$order_product['orders_id'] = $order_id;

		$this->db->insert('orders_products', $order_product);

		return $this->db->insert_id();
    }

    public function get_pos_locations_id_from_locations_id($locations_id)
	{
		$this->db->select('pos_locations_id');
		$this->db->from('locations');
		$this->db->where('id', $locations_id);
		$this->db->limit(1);

		$get_location = $this->db->get();

		if ($get_location->num_rows() > 0) {

			$location = $get_location->row_array();

			return $location['pos_locations_id'];

		} else {

			return FALSE;

		}

	}

    public function get_pos_products_id_from_products_id($products_id)
	{
		$this->db->select('pos_products_id');
		$this->db->from('products');
		$this->db->where('id', $products_id);
		$this->db->limit(1);

		$get_product = $this->db->get();

		if ($get_product->num_rows() > 0) {

			$product = $get_product->row_array();

			return $product['pos_products_id'];

		} else {

			return FALSE;

		}

	}

	public function get_pos_portions_id_from_portions_id($portions_id)
	{
		$this->db->select('pos_portions_id');
		$this->db->from('portions');
		$this->db->where('id', $portions_id);
		$this->db->limit(1);

		$get_portion = $this->db->get();

		if ($get_portion->num_rows() > 0) {

			$portion = $get_portion->row_array();

			return $portion['pos_portions_id'];

		} else {

			return FALSE;

		}

	}

	public function get_order_by_id($orders_id)
	{
		$this->load->model('v1/customers_model');
		$this->load->model('v1/locations_model');
		$this->load->model('v1/transactions_model');

		$this->db->select();
		$this->db->from('orders');
		$this->db->where('id', $orders_id);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			$result['status_current'] = $this->get_current_order_status($orders_id);
			$result['location'] = $this->locations_model->get_location_by_id($result['locations_id']);
			$result['customer'] = $this->customers_model->get_customer_by_id($result['customers_id']);
			$result['products'] = $this->get_orders_products($orders_id);
			$result['transaction'] = $this->transactions_model->get_transaction_by_order_id($orders_id);

			return $result;

		} else {

			return FALSE;

		}

	}
	

	public function get_orders_products($orders_id)
	{
		$this->db->select('p.id, p.name, p.description, portions.name as portion_name, portions.id as portion_id, op.quantity, op.unit_price, op.total_price, op.pos_sku');
		$this->db->from('orders_products as op');
		$this->db->where('op.orders_id', $orders_id);
		$this->db->join('products as p', 'op.products_id = p.id', 'left');
		$this->db->join('portions', 'op.portions_id = portions.id', 'left');
		$this->db->group_by('p.id');

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result_array();

			return $result;

		} else {

			return FALSE;

		}

	}	

	public function get_current_order_status($orders_id)
	{
		$this->db->select('os.id, os.name, os.description, osh.date_added');
		$this->db->from('orders_statuses_history as osh');
		$this->db->where('orders_id', $orders_id);
		$this->db->order_by('date_added', 'desc');
		$this->db->limit(1);
		$this->db->join('orders_statuses as os', 'osh.orders_statuses_id = os.id', 'left');

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			return $result;

		} else {

			return FALSE;

		}

	}

	public function add_orders_statuses_history($orders_id, $orders_statuses_id, $date_added = FALSE)
    {
		$db_data['orders_id'] = $orders_id;
		$db_data['orders_statuses_id'] = $orders_statuses_id;
		
		if (!$date_added) {

			$db_data['date_added'] = date('Y-m-d H:i:s');

		}

		$this->db->insert('orders_statuses_history', $db_data);

		return $this->db->insert_id();
    }

    public function get_order_status_by_id($orders_statuses_id)
    {
		$this->db->select();
		$this->db->from('orders_statuses');
		$this->db->where('id', $orders_statuses_id);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			return $result;

		} else {

			return FALSE;

		}

    }

}