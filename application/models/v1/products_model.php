<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function update_location_pos_total_available($db_data)
	{
		if ((isset($db_data['pos_locations_id'])) && (isset($db_data['pos_products_id'])) && (isset($db_data['pos_portions_id'])) && (isset($db_data['pos_total_available']))) {

			$db_data['locations_id'] = $this->get_locations_id_from_pos_locations_id($db_data['pos_locations_id']);
			$db_data['products_id'] = $this->get_products_id_from_pos_products_id($db_data['pos_products_id']);
			$db_data['portions_id'] = $this->get_portions_id_from_pos_portions_id($db_data['pos_portions_id']);

			$this->db->select('id');
			$this->db->from('locations_products');
			$this->db->where('pos_locations_id', $db_data['pos_locations_id']);
			$this->db->where('pos_products_id', $db_data['pos_products_id']);
			$this->db->where('pos_portions_id', $db_data['pos_portions_id']);

			$get_locations_products = $this->db->get();

			if ($get_locations_products->num_rows() > 0) {

				$locations_products = $get_locations_products->row_array();

				$this->db->where('id', $locations_products['id']);
				$this->db->update('locations_products', $db_data);

			} else {

				$this->db->insert('locations_products', $db_data);

			}

		}

	}

	function get_locations_id_from_pos_locations_id($pos_locations_id)
	{
		$this->db->select('id');
		$this->db->from('locations');
		$this->db->where('pos_locations_id', $pos_locations_id);
		$this->db->limit(1);

		$get_location = $this->db->get();

		if ($get_location->num_rows() > 0) {

			$location = $get_location->row_array();

			return $location['id'];

		} else {

			return FALSE;

		}

	}

	function get_products_id_from_pos_products_id($pos_products_id)
	{
		$this->db->select('id');
		$this->db->from('products');
		$this->db->where('pos_products_id', $pos_products_id);
		$this->db->limit(1);

		$get_product = $this->db->get();

		if ($get_product->num_rows() > 0) {

			$product = $get_product->row_array();

			return $product['id'];

		} else {

			return FALSE;

		}

	}

	function get_portions_id_from_pos_portions_id($pos_portions_id)
	{
		$this->db->select('id');
		$this->db->from('portions');
		$this->db->where('pos_portions_id', $pos_portions_id);
		$this->db->limit(1);

		$get_portion = $this->db->get();

		if ($get_portion->num_rows() > 0) {

			$portion = $get_portion->row_array();

			return $portion['id'];

		} else {

			return FALSE;

		}

	}

	function get_product_by_sku($sku)
	{
		$pos_products_id = substr($sku, 0, 4); // The first four digits are the product ID.
		$pos_portions_id = substr($sku, 4, 1); // The fifth digit is the portion ID.

		$this->db->select('products.status, products.start_date, products.end_date, products_portions.price');

		$this->db->from('products');

		$this->db->where('products.pos_products_id', $pos_products_id);
		$this->db->where('portions.pos_portions_id', $pos_portions_id);

		$this->db->join('products_portions', 'products.id = products_portions.products_id', 'left');
		$this->db->join('portions', 'products_portions.portions_id = portions.id', 'left');

		$this->db->limit(1);

		$get_product = $this->db->get();

		if ($get_product->num_rows() > 0) {

			$product = $get_product->row_array();

			return $product;

		} else {

			return FALSE;

		}

	}

}