<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Locations_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function get_location_by_id($id)
    {
		$this->db->select();
		$this->db->from('locations');
		$this->db->where('id', $id);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

		} else {

			$result = FALSE;

		}

        return $result;
    }

}