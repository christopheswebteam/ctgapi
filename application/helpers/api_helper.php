<?php

function validate_field($field_name, $field_label, $field_rules)
{
	$ci =& get_instance();
	
	$ci->form_validation->set_rules($field_name, $field_label, $field_rules);
}

function run_validation()
{
	$ci =& get_instance();
	
	$ci->form_validation->run();
}

?>