<?php

class Products extends REST_Controller
{

	public $methods = array(
		'update_location_pos_total_available_get' => array('level' => 10, 'limit' => 10),
		'verify_sku_get' => array('level' => 10, 'limit' => 10)
	);

	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->helper('api');
	}

	function update_location_pos_total_available_get()
	{
		$this->load->model('v1/products_model');

		validate_field('pos_locations_id', 'POS Locations ID', 'required|trim|integer');
		validate_field('pos_sku', 'POS SKU', 'trim|integer');
		validate_field('pos_total_available', 'POS Total Available', 'required|trim|integer');

		run_validation();

		if (validation_errors()) {

			$data['error'] = $this->validation_errors();
			$data['status'] = FALSE;

		} else {

			$sku = $this->get('pos_sku');

			$pos_products_id = substr($sku, 0, 4); // The first four digits are the product ID.
			$pos_portions_id = substr($sku, 4, 1); // The fifth digit is the portion ID.

			$db_data['pos_locations_id'] = $this->get('pos_locations_id');
			$db_data['pos_products_id'] = $pos_products_id;
			$db_data['pos_portions_id'] = $pos_portions_id;
			$db_data['pos_total_available'] = $this->get('pos_total_available');
			$db_data['pos_total_available_date'] = date('Y-m-d H:i:s');

			$this->products_model->update_location_pos_total_available($db_data);

			$data['status'] = TRUE;
			$data['message'] = "The total amount of product ID ".$db_data['pos_products_id']." and portion ID ".$db_data['pos_portions_id']." for location ID ".$db_data['pos_locations_id']." has been updated to ".$db_data['pos_total_available'].".";

		}

		$this->response($data);
		
	}

	function verify_sku_get()
	{
		$this->load->model('v1/products_model');

		if ((!isset($_GET['pos_sku'])) || (!isset($_GET['price'])) || (!isset($_GET['status']))) {

			$data['error'] = "All fields are required.";
			$data['status'] = FALSE;

		} else {

			$sku = $this->get('pos_sku');
			$price = $this->get('price');
			$status = $this->get('status');

			$product = $this->products_model->get_product_by_sku($sku);

			if ($product) {

				if ($status == 1) {

					if (($product['start_date']) && ($product['end_date'])) {

						if (($product['status'] == 1) && (strtotime($product['start_date']) <= strtotime(date('Y-m-d'))) && (strtotime($product['end_date']) >= strtotime(date('Y-m-d'))) && ($price == $product['price'])) {

							$data['status'] = TRUE;
							$data['message'] = "This SKU is confirmed.";

						} elseif ($product['status'] == 0) {

							$data['status'] = FALSE;
							$data['message'] = "This SKU status is set to Disabled.";

						} elseif ((strtotime($product['start_date']) >= strtotime(date('Y-m-d'))) || (strtotime($product['end_date']) <= strtotime(date('Y-m-d')))) {

							$data['status'] = FALSE;
							$data['message'] = "This SKU is not active due to the available date range of ".$product['start_date']." through ".$product['end_date'].".";

						} elseif ($price != $product['price']) {

							$data['status'] = FALSE;
							$data['message'] = "The price does not match. The web has this SKU listed for $".$product['price'].".";

						}

					} else {

						if (($product['status'] == 1) && ($price == $product['price'])) {

							$data['status'] = TRUE;
							$data['message'] = "This SKU is confirmed.";

						} elseif ($product['status'] == 0) {

							$data['status'] = FALSE;
							$data['message'] = "This SKU status is set to Disabled.";

						} elseif ($price != $product['price']) {

							$data['status'] = FALSE;
							$data['message'] = "The price does not match. The web has this SKU listed for $".$product['price'].".";

						}

					}

				} elseif ($status == 0) {

					if (($product['start_date']) && ($product['end_date'])) {

						// If the End Date has passed then the site should consider this SKU Disabled regardless of the status column.
						if ((strtotime($product['end_date']) <= strtotime(date('Y-m-d'))) && ($price == $product['price'])) {

							$data['status'] = TRUE;
							$data['message'] = "This SKU is confirmed.";

						} elseif ($product['status'] == 1) {

							$data['status'] = FALSE;
							$data['message'] = "This SKU status is set to Enabled.";

						} elseif ((strtotime($product['start_date']) <= strtotime(date('Y-m-d'))) && (strtotime($product['end_date']) >= strtotime(date('Y-m-d')))) {

							$data['status'] = FALSE;
							$data['message'] = "This SKU is active due to the available date range of ".$product['start_date']." through ".$product['end_date'].".";

						} elseif ($price != $product['price']) {

							$data['status'] = FALSE;
							$data['message'] = "The price does not match. The web has this SKU listed for $".$product['price'].".";

						}

					} else {

						if (($product['status'] == 1) && ($price == $product['price'])) {

							$data['status'] = TRUE;
							$data['message'] = "This SKU is confirmed.";

						} elseif ($product['status'] == 1) {

							$data['status'] = FALSE;
							$data['message'] = "This SKU status is set to Enabled.";

						} elseif ($price != $product['price']) {

							$data['status'] = FALSE;
							$data['message'] = "The price does not match. The web has this SKU listed for $".$product['price'].".";

						}

					}

				}

			} else {

				$data['error'] = "SKU not found.";
				$data['status'] = FALSE;

			}
			
		}

		$this->response($data);
		
	}

}