<?php

class Orders extends REST_Controller
{

	public $methods = array(
		'update_location_pos_total_available_get' => array('level' => 10, 'limit' => 10)
	);

	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->helper('api');
	}
	
	function send_order_to_pos_post()
	{
		$this->load->library('pos_api');
	
		$this->load->model('v1/orders_model');
		
		validate_field('order_id', 'Order ID', 'trim');
		
		run_validation();

		if (validation_errors()) {

			$data['error'] = $this->validation_errors();
			$data['status'] = FALSE;

		} else {

			$pos_data = array();
			
			$order_id = $this->post('order_id');
			
			$order = $this->orders_model->get_order_by_id($order_id);
			
			if ($order) {
			
				// Customer Information
		
				$order_data['customer_id'] = $order['customer']['id'];
				$order_data['name_first'] = $order['customer']['name_first'];
				$order_data['name_last'] = $order['customer']['name_last'];
				$order_data['email_address'] = $order['customer']['email_address'];
				$order_data['phone_number'] = $order['customer']['phone_number'];
				$order_data['street_address_1'] = $order['customer']['street_address_1'];
				$order_data['street_address_2'] = $order['customer']['street_address_2'];
				$order_data['city'] = $order['customer']['city'];
				$order_data['state'] = $order['customer']['state'];
				$order_data['zip_code'] = $order['customer']['zip_code'];
				
				// Order Information
				
				$order_data['order_id'] = $order['id'];
				$order_data['subtotal'] = $order['subtotal'];
				$order_data['tax'] = $order['tax'];
				$order_data['total'] = $order['total'];
				$order_data['pickup_date'] = $order['pickup_date'];
				$order_data['pickup_time'] = $order['pickup_time'];
				$order_data['locations_id'] = $order['locations_id'];
				$order_data['pos_locations_id'] = $this->orders_model->get_pos_locations_id_from_locations_id($order_data['locations_id']);
				$order_data['date_added'] = date('Y-m-d H:i:s');
				
				// Products Information
				
				$i = 0;
				
				foreach ($order['products'] as $product) {
					
					$key = $i++;
					
					$order_data['products'][$key]['quantity'] = $product['quantity'];
					$order_data['products'][$key]['products_id'] = $product['id'];
					$order_data['products'][$key]['pos_products_id'] = $this->orders_model->get_pos_products_id_from_products_id($order_data['products'][$key]['products_id']);
					$order_data['products'][$key]['portions_id'] = $product['portion_id'];
					$order_data['products'][$key]['pos_portions_id'] = $this->orders_model->get_pos_portions_id_from_portions_id($order_data['products'][$key]['portions_id']);
					$order_data['products'][$key]['pos_sku'] = $order_data['products'][$key]['pos_products_id'].$order_data['products'][$key]['pos_portions_id'];
					$order_data['products'][$key]['unit_price'] = $product['unit_price'];
					$order_data['products'][$key]['total_price'] = $order_data['products'][$key]['quantity'] * $order_data['products'][$key]['unit_price'];
				
				}
				
				// Transaction Information
				
				if ($order['transaction']) {
				
					$order_data['transaction_id'] = $order['transaction']['id'];
					$order_data['transaction_amount'] = $order['transaction']['amount'];
					$order_data['transaction_status'] = 1;
				
				} else {
				
					$order_data['transaction_id'] = FALSE;
					$order_data['transaction_amount'] = FALSE;
					$order_data['transaction_status'] = 0;
				
				}
				
				$this->pos_api->submit_order($order_data);
				
				$data['message'] = "Order sent to the POS successfully.";
				$data['status'] = TRUE;

			} else {
			
				$data['error'][] = "Can not find order ID ".$order_id.".";
				$data['status'] = FALSE;
			
			}

		}

		$this->response($data);
		
	}
	
	function submit_order_to_pos_post()
	{        
		$this->load->library('pos_api');
	
		$this->load->model('v1/orders_model');
		$this->load->model('v1/transactions_model');

		// Customer Fields
		validate_field('name_first', 'Customer First Name', 'required|trim');
		validate_field('name_last', 'Customer Last Name', 'required|trim');
		validate_field('email_address', 'Customer E-Mail Address', 'required|trim|valid_email|strtolower');
		validate_field('phone_number', 'Customer Phone Number', 'trim');
		validate_field('street_address_1', 'Customer Street Address 1', 'required|trim');
		validate_field('street_address_2', 'Customer Street Address 2', 'trim');
		validate_field('city', 'Customer City', 'required|trim');
		validate_field('state', 'Customer State', 'required|trim');
		validate_field('zip_code', 'Customer Zip Code', 'required|trim');

		// Order Fields
		validate_field('order_id', 'Order ID', 'trim');
		validate_field('order_subtotal', 'Order Sub-Total', 'required|trim');
		validate_field('order_tax', 'Order Tax', 'required|trim');
		validate_field('order_total', 'Order Total', 'required|trim');
		validate_field('order_pickup_date', 'Order Pickup Date', 'required|trim');
		validate_field('order_pickup_time', 'Order Pickup Time', 'trim');
		validate_field('order_locations_id', 'Order Location ID', 'required|trim');

		// Credit Card Fields
		validate_field('cc_name_first', 'Credit Card First Name', 'required|trim');
		validate_field('cc_name_last', 'Credit Card Last Name', 'required|trim');
		validate_field('cc_street_address_1', 'Credit Card Street Address 1', 'required|trim');
		validate_field('cc_street_address_2', 'Credit Card Street Address 2', 'trim');
		validate_field('cc_city', 'Credit Card City', 'required|trim');
		validate_field('cc_state', 'Credit Card State', 'required|trim');
		validate_field('cc_zip_code', 'Credit Card Zip Code', 'required|trim');
		validate_field('cc_type', 'Credit Card Type', 'required|trim');
		validate_field('cc_number', 'Credit Card Number', 'required|trim');
		validate_field('cc_cvn', 'Credit Card Verification Number', 'required|trim');
		validate_field('cc_exp_month', 'Credit Card Expiration Month', 'required|trim');
		validate_field('cc_exp_year', 'Credit Card Expiration Year', 'required|trim');

		run_validation();

		if (validation_errors()) {

			$data['error'] = $this->validation_errors();
			$data['status'] = FALSE;

		} else {

			$pos_data = array();

			// First we add or update the customer record so we have a customer ID.
			
			$customer_data = $this->_upsert_customer_from_post();

			if ($customer_data['status']) {
				
				// Add customer_id to the API response for use on checkout page.
				$data['data']['customer_id'] = $customer_data['data']['customer_id'];
				
				// Add the customer information to the POS data array.
				$pos_data += $customer_data['data'];

				// Order Fields
				
				$order_data = $this->_upsert_customer_order_from_post($customer_data['data']['customer_id']);

				if ($order_data['status']) {
					
					// Add order_id to the API response for use on checkout page.
					$data['data']['order_id'] = $order_data['data']['order_id'];
					
					// Add the order information to the POS data array.
					$pos_data += $order_data['data'];
					
					// Product Fields
						
					$order_products_data = $this->_upsert_customer_order_products_from_post($order_data['data']['order_id']);

					$pos_data['products'] = $order_products_data;

					// Credit Card Fields
					
					$cc_data = $this->_upsert_credit_card_from_post($customer_data['data']['customer_id']);
					
					if ($cc_data['status']) {

						// If pickup date is not today then we need to charge the credit card for total amount.
						if ($order_data['data']['pickup_date'] != date('Y-m-d')) {
							
							// If order is placed after 12:00PM (noon) for pickup tomorrow then don't charge the card. Treat as a same day order.
							if ((strtotime(date('h:i A')) > strtotime('12:00 PM')) && ($order_data['data']['pickup_date'] == date('Y-m-d', strtotime('+1 day')))) {
				
								$transaction_data['transaction_id'] = FALSE;
								$transaction_data['transaction_amount'] = FALSE;
								$transaction_data['transaction_status'] = 0;
								
							} else {
								
								$transaction_id = $this->transactions_model->process_order_transaction($order_data['data']['order_id'], $cc_data['data']['payment_profiles_id'], $this->post('cc_cvn'));
								
								if ($transaction_id) {
							 
									$transaction_data['transaction_id'] = $transaction_id;
									$transaction_data['transaction_amount'] = $order_data['data']['total'];
									$transaction_data['transaction_status'] = 1;
									
								} else {
								
									$data['error'][] = "Credit card declined. Please try again.";
									$data['status'] = FALSE;
									
									$this->response($data);

								}
							
							}

						} else {

							$transaction_data['transaction_id'] = FALSE;
							$transaction_data['transaction_amount'] = FALSE;
							$transaction_data['transaction_status'] = 0;

						}
		
						$pos_data += $cc_data['data'];

						$pos_data += $transaction_data;

						$this->pos_api->submit_order($pos_data);
						
						// Determine which status to set so that proper e-mail gets sent.
						if ($order_data['data']['pickup_date'] != date('Y-m-d')) {
							
							// If order is placed after 12:00PM (noon) for pickup tomorrow then use status 2.
							if ((strtotime(date('h:i A')) > strtotime('12:00 PM')) && ($order_data['data']['pickup_date'] == date('Y-m-d', strtotime('+1 day')))) {
				
								$this->orders_model->add_orders_statuses_history($order_data['data']['order_id'], 2);

								$this->_email_customer_status_update($order_data['data']['order_id'], 2);
							
							// If order is placed before 12:00PM (noon) and/or is scheduled for future pickup then use status 15 (confirm the order immediately).
							} else {
							
								$this->orders_model->add_orders_statuses_history($order_data['data']['order_id'], 15);

								$this->_email_customer_status_update($order_data['data']['order_id'], 15);
							
							}

						} else {

							$this->orders_model->add_orders_statuses_history($order_data['data']['order_id'], 1);

							$this->_email_customer_status_update($order_data['data']['order_id'], 1);

						}

						$data['status'] = TRUE;
						$data['message'] = "The order has been successfully sent to the POS.";
						$data['data'] = $pos_data;

					} else {
					
						$data['status'] = FALSE;
						$data['error'] = $cc_data['error'];
					
					}

				} else {

					$data['error'][] = "Successfully added the customer record but error while adding the customer order to the database.";
					$data['status'] = FALSE;

				}

			} else {

				$data['error'][] = "Error while upserting the customer record.";
				$data['status'] = FALSE;

			}

		}

		$this->response($data);

	}
	
	private function _upsert_customer_from_post()
	{
		$this->load->model('v1/customers_model');
		
		$customer_data['name_first'] = $this->post('name_first');
		$customer_data['name_last'] = $this->post('name_last');
		$customer_data['email_address'] = $this->post('email_address');
		$customer_data['street_address_1'] = $this->post('street_address_1');
		$customer_data['street_address_2'] = $this->post('street_address_2');
		$customer_data['city'] = $this->post('city');
		$customer_data['state'] = $this->post('state');
		$customer_data['zip_code'] = $this->post('zip_code');
		$customer_data['phone_number'] = $this->post('phone_number');

		$customer_id = $this->customers_model->upsert_customer_by_email($customer_data);
		
		if ($customer_id) {
		
			$customer_data['customer_id'] = $customer_id;
			
			$data['status'] = TRUE;
			$data['data'] = $customer_data;
			
		} else {
		
			$data['status'] = FALSE;
			
		}
		
		return $data;
	}
	
	private function _upsert_customer_order_from_post($customer_id)
	{
		$this->load->model('v1/orders_model');
		
		$order_data['locations_id'] = $this->post('order_locations_id');
		$order_data['pickup_date'] = $this->post('order_pickup_date');
		$order_data['pickup_time'] = $this->post('order_pickup_time');
		$order_data['subtotal'] = $this->post('order_subtotal');
		$order_data['tax'] = $this->post('order_tax');
		$order_data['total'] = $this->post('order_total');
	
		if ($this->post('order_id')) {
			
			$order_id = $this->orders_model->update_customer_order($this->post('order_id'), $customer_id, $order_data);
			
		} else {
		
			$order_id = $this->orders_model->insert_customer_order($customer_id, $order_data);
			
		}
		
		// Manually set data for the POS to have.
		$order_data['order_id'] = $order_id;
		$order_data['pos_locations_id'] = $this->orders_model->get_pos_locations_id_from_locations_id($order_data['locations_id']);
		$order_data['date_added'] = date('Y-m-d H:i:s');
		
		$data['status'] = TRUE;
		$data['data'] = $order_data;
		
		return $data;
	}
	
	private function _upsert_customer_order_products_from_post($orders_id)
	{
		$this->load->model('v1/orders_model');
		
		$this->orders_model->clear_orders_products($orders_id);
		
		$order_products = $this->post('products');

		if (($order_products) && (is_array($order_products))) {

			$o = 0;

			foreach ($order_products as $order_product) {

				$key = $o++;

				$order_products_data[$key]['quantity'] = $order_product['quantity'];
				$order_products_data[$key]['products_id'] = $order_product['products_id'];
				$order_products_data[$key]['pos_products_id'] = $this->orders_model->get_pos_products_id_from_products_id($order_products_data[$key]['products_id']);
				$order_products_data[$key]['portions_id'] = $order_product['portions_id'];
				$order_products_data[$key]['pos_portions_id'] = $this->orders_model->get_pos_portions_id_from_portions_id($order_products_data[$key]['portions_id']);
				$order_products_data[$key]['pos_sku'] = $order_products_data[$key]['pos_products_id'].$order_products_data[$key]['pos_portions_id'];
				$order_products_data[$key]['unit_price'] = $order_product['unit_price'];
				$order_products_data[$key]['total_price'] = $order_products_data[$key]['quantity'] * $order_products_data[$key]['unit_price'];

				$this->orders_model->insert_orders_product($orders_id, $order_products_data[$key]);

			}

			return $order_products_data;

		} else {
		
			return FALSE;
		
		}
	}
	
	private function _upsert_credit_card_from_post($customer_id)
	{
		$this->load->model('v1/payment_profiles_model');
		
		$cc_data['cc_name_first'] = $this->post('cc_name_first');
		$cc_data['cc_name_last'] = $this->post('cc_name_last');
		$cc_data['cc_street_address_1'] = $this->post('cc_street_address_1');
		$cc_data['cc_street_address_2'] = $this->post('cc_street_address_2');
		$cc_data['cc_city'] = $this->post('cc_city');
		$cc_data['cc_state'] = $this->post('cc_state');
		$cc_data['cc_zip_code'] = $this->post('cc_zip_code');
		$cc_data['cc_type'] = $this->post('cc_type');
		$cc_data['cc_number'] = $this->post('cc_number');
		$cc_data['cc_cvn'] = $this->post('cc_cvn');
		$cc_data['cc_exp_month'] = $this->post('cc_exp_month');
		$cc_data['cc_exp_year'] = $this->post('cc_exp_year');

		$validate_cc_data = $this->_validate_cc_data($cc_data['cc_number'], $cc_data['cc_cvn'], $cc_data['cc_exp_year'].'-'.$cc_data['cc_exp_month']);

		if (!is_array($validate_cc_data)) {

			$data['error'][] = $validate_cc_data;
			$data['status'] = FALSE;
			
			return $data;
			
		} else {

			$cc_data['cc_number'] = $validate_cc_data[0];
			$cc_data['cc_cvn'] = FALSE; //$validate_cc_data[1]; // Dan requested we don't send CVN to POS anymore for PCI compliance.

			$cc_exp_date = explode('-', $validate_cc_data[2]);

			$cc_data['cc_exp_month'] = $cc_exp_date[1];
			$cc_data['cc_exp_year'] = $cc_exp_date[0];
			
			$payment_profiles_id = $this->payment_profiles_model->upsert_credit_card($customer_id, $cc_data);
			
			if ($payment_profiles_id) {
				
				$data['data'] = $cc_data;
				$data['data']['payment_profiles_id'] = $payment_profiles_id;
				
				$data['status'] = TRUE;
			
			} else {
			
				$data['error'][] = "We were not able to validate the credit card you entered. Please try again.";
				
				$data['status'] = FALSE;

			}
			
			return $data;
		}
	}
	
	private function _validate_cc_data($cc_number, $cc_cvn, $cc_exp_date)
	{
		// Strip out non-numeric values (dashes, spaces, hyphens, etc.).
		$cc_number = preg_replace('/\D/', '', $cc_number);

		// Credit card number should be between 13 to 16 digits.
		if (!((strlen($cc_number) >= 13) && (strlen($cc_number) <= 16))) {
			return "Credit card number must be between 13 and 16 digits.";
		}

	    $first_number = (int)substr($cc_number, 0, 1);

		switch($first_number) {
	        case 3:
	            if (!preg_match('/^3\d{3}[ \-]?\d{6}[ \-]?\d{5}$/', $cc_number)) {
	                return "This is not a valid American Express card number.";
	            }
	            break;
	        case 4:
	            if (!preg_match('/^4\d{3}[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $cc_number)) {
	                return "This is not a valid Visa card number.";
	            }
	            break;
	        case 5:
	            if (!preg_match('/^5\d{3}[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $cc_number)) {
	                return "This is not a valid MasterCard card number.";
	            }
	            break;
	        case 6:
	            if (!preg_match('/^6011[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $cc_number)) {
	                return "This is not a valid Discover card number.";
	            }
	            break;
	        default:
	            return 'This is not a valid credit card number';
	            break;
	    }

		// Luhn Algorithm (http://en.wikipedia.org/wiki/Luhn_algorithm)
		
		settype($cc_number, 'string');
		
		$sumTable = array(array(0,1,2,3,4,5,6,7,8,9), array(0,2,4,6,8,1,3,5,7,9));
		
		$sum = 0;
		$flip = 0;
		
		for ($i = strlen($cc_number) - 1; $i >= 0; $i-- ) {
			$sum += $sumTable[$flip++ & 0x1][$cc_number[$i]];
		}

		if ($sum % 10 !== 0) {
			return "This is not a valid credit card number.";
		}

		// Validate CVN
		
	    if ($first_number === 3) {
		
	        if (!preg_match("/^\d{4}$/", $cc_cvn)) {
			
	            // The credit card is an American Express card but does not have a four digit CVV code.
	            return "The security code for American Express cards must be 4 digits.";
				
	        }
			
	    } elseif (!preg_match("/^\d{3}$/", $cc_cvn)) {
		
	        // The credit card is a Visa, MasterCard, or Discover Card card but does not have a three digit CVV code.
	        return "The security code for this card must be 3 digits.";
	    }

	   	// Validate expiration date.
		
		list($year, $month) = explode('-', $cc_exp_date);

		if (!preg_match('/^\d{2}$/', $month)){
		
	        return "Expiration month is not valid.";
			
	    } elseif (!preg_match('/^\d{4}$/', $year)) {
		
	        return "Expiration year is not valid.";
			
	    } elseif ($year < date('Y')) {
		
	        return "Card has expired.";
			
	    } elseif (($month < date('m')) && ($year == date('Y'))) {
		
	        return "Card has expired.";
			
	    }

	    $cc_exp_date = $year."-".$month;

	    // Return the cleaned data if we have made it here.
	    return array($cc_number, $cc_cvn, $cc_exp_date);
	}

	function update_status_get()
	{
		$this->load->model('v1/orders_model');

		if ((!isset($_GET['orders_id'])) || (!isset($_GET['orders_statuses_id']))) {

			$data['error'] = "All fields are required.";
			$data['status'] = FALSE;

		} else {

			$orders_id = $this->get('orders_id');
			$orders_statuses_id = $this->get('orders_statuses_id');

			$order = $this->orders_model->get_order_by_id($orders_id);

			if ($order) {

				$order_status = $this->orders_model->get_current_order_status($orders_id);

				if ($order_status) {

					if ($order_status['id'] == $orders_statuses_id) {

						$data['error'] = "This order status ID is already set to ".$order_status['id'].".";
						$data['status'] = FALSE;

					} else {

						$this->orders_model->add_orders_statuses_history($orders_id, $orders_statuses_id);

						$this->_email_customer_status_update($orders_id, $orders_statuses_id);

						$data['status'] = TRUE;
						$data['message'] = "The order status ID has been successfully updated to ".$orders_statuses_id.".";

					}

				} else {

					$this->orders_model->add_orders_statuses_history($orders_id, $orders_statuses_id);

					$this->_email_customer_status_update($orders_id, $orders_statuses_id);

					$data['status'] = TRUE;
					$data['message'] = "The order status ID has been successfully updated to ".$orders_statuses_id.".";

				}

			} else {

				$data['error'] = "Can not find an order with ID ".$orders_id.".";
				$data['status'] = FALSE;

			}

		}

		$this->response($data);

	}

	function _email_customer_status_update($orders_id, $orders_statuses_id)
	{
		$this->load->library('email');
		$this->load->model('v1/orders_model');

		$order_status = $this->orders_model->get_order_status_by_id($orders_statuses_id);

		if (($order_status) && ($order_status['email_subject']) && ($order_status['email_template'])) {

			$order = $this->orders_model->get_order_by_id($orders_id);

			if (($order) && (isset($order['customer']['email_address'])) && ($order['customer']['email_address'])) {

				$email_data['order'] = $order;

				$this->email->set_mailtype('html');

				$this->email->from('orders@christophestogo.com', 'Christophe\'s To Go');
				$this->email->to($order['customer']['email_address'], $order['customer']['name_first'].' '.$order['customer']['name_last']);
				$this->email->subject($order_status['email_subject']);
				$this->email->message($this->load->view('email_templates/'.$order_status['email_template'], $email_data, TRUE));	
				$this->email->send();

			}

		}

	}
}