<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sandbox extends MY_Controller
{
	public function mandrill_test()
	{
		$this->load->library('mandrill');

		//Send us some email!
		$email = array(
			'html' => '<p>Let Jay know if you received this e-mail. Testing Mandrill again.<p>', //Consider using a view file
			'text' => 'Let Jay know if you received this e-mail. Testing Mandrill again.',
			'subject' => 'E-Mail Test Again',
			'from_email' => 'orders@christophestogo.com',
			'from_name' => 'Christophe\'s To Go',
			'to' => array(array('email' => 'cyclone@christophestogo.com')) //Check documentation for more details on this one
			//'to' => array(array('email' => 'joe@example.com' ),array('email' => 'joe2@example.com' )) //for multiple emails
		);

		$result = $this->mandrill->messages_send($email);
		
		echo "<pre>";
		print_r($result);
		echo "</pre>";
	}
	
	public function locations_products()
	{
		$this->db->select();
		$this->db->from('locations_products');

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->result_array();

			echo "<table border=\"1\">";

			echo "<tr>";
			echo "<th>Location ID</th>";
			echo "<th>Product ID</th>";
			echo "<th>Portion ID</th>";
			echo "<th>Total Available</th>";
			echo "<th>Last Update</th>";
			echo "</tr>";

			foreach ($result as $row) {

				echo "<tr>";
				echo "<td>".$row['pos_locations_id']."</td>";
				echo "<td>".$row['pos_products_id']."</td>";
				echo "<td>".$row['pos_portions_id']."</td>";
				echo "<td>".$row['pos_total_available']."</td>";
				echo "<td>".$row['pos_total_available_date']."</td>";
				echo "</tr>";

			}

			echo "</table>";

		}

	}

	public function specific_locations_products($pos_locations_id, $pos_products_id, $pos_portions_id)
	{
		$this->db->select('pos_total_available, pos_total_available_date');
		$this->db->from('locations_products');
		$this->db->where('pos_locations_id', $pos_locations_id);
		$this->db->where('pos_products_id', $pos_products_id);
		$this->db->where('pos_portions_id', $pos_portions_id);

		$query = $this->db->get();

		if ($query->num_rows() > 0) {

			$result = $query->row_array();

			echo $result['pos_total_available'];

		} else {
			
			echo "No data found.";
		
		}

	}

	public function authorizenet_test()
	{
		$this->load->library('authorizenet', array('98aD6W6sW', '5B92dHs2B57uX22G', FALSE));

		$this->authorizenet->createCustomerProfileRequest(array(
			'profile'	=>	array(
				'merchantCustomerId'	=>	666666,
				'email'					=>	'jaylogan@fucoso.com'
			)
		));

		if ($this->authorizenet->isSuccessful()) {

			echo (int)$this->authorizenet->customerProfileId;

		} elseif ($this->authorizenet->isError()) {

			print_r($this->authorizenet->response_xml);

		}

	}

	public function authorizenet_test2()
	{
		$this->load->library('authorizenet_cim');

		$this->authorizenet_cim->set_data('email', 'jaylogan@jaylogan.com');
    	$this->authorizenet_cim->set_data('description', 'Online ordering customer.');
    	$this->authorizenet_cim->set_data('merchantCustomerId', 123456);

		$cim_id = $this->authorizenet_cim->create_customer_profile();

		//exit($cim_id);

		echo $this->authorizenet_cim->get_error_msg();
	}
	
	public function show_order($orders_id)
	{
		$this->load->model('v1/orders_model');
		
		$order = $this->orders_model->get_order_by_id($orders_id);
		
		echo "<pre>";
		print_r($order);
		echo "</pre>";
	}
	
	function email_test($orders_id, $orders_statuses_id)
	{
		$this->load->library('email');
		$this->load->model('v1/orders_model');

		$order_status = $this->orders_model->get_order_status_by_id($orders_statuses_id);

		if (($order_status) && ($order_status['email_subject']) && ($order_status['email_template'])) {

			$order = $this->orders_model->get_order_by_id($orders_id);

			if (($order) && (isset($order['customer']['email_address'])) && ($order['customer']['email_address'])) {

				$email_data['order'] = $order;

				$this->email->set_mailtype('html');

				$this->email->from('orders@christophestogo.com', 'Christophe\'s To Go');
				$this->email->to($order['customer']['email_address'], $order['customer']['name_first'].' '.$order['customer']['name_last']);
				$this->email->subject($order_status['email_subject']);
				$this->email->message($this->load->view('email_templates/'.$order_status['email_template'], $email_data, TRUE));	
				$this->email->send();

			}
			
			echo "<pre>";
			print_r($order);
			echo "</pre>";

		}

	}
}

/* End of file sandbox.php */
/* Location: ./application/controllers/sandbox.php */