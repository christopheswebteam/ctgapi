<?php

class Pos_api
{

	public function submit_order($params)
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://jc.christophestogo.com:6853/formdump.asp');
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$response = curl_exec($ch);

		curl_close($ch);

		return $response;
	}

}

?>