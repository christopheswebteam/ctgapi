<?php echo $this->load->view('email_templates/includes/header', FALSE, TRUE); ?>

<table bgcolor="#99FF99" style="padding:20px;margin:20px 0px;width:400px;">
    <tr>
        <td>
            <b>Order Confirmed – Pickup <?php echo date('m/d/Y', strtotime($order['pickup_date'])); ?>!</b>
        </td>
    </tr>
</table>

<p>
	Hello <b><?php echo $order['customer']['name_first']; ?></b>. We are happy to report that your online order has been confirmed and your credit card has been charged. Your order will be ready for pick up at the <?php echo $order['location'][ 'name']; ?> store on <?php echo date('m/d/Y', strtotime($order['pickup_date'])); ?> between <?php echo $order['location']['opening_time']; ?> and <?php echo $order['location']['closing_time']; ?>.<br />
    <br />
	The details of your order are below. Please check the order. If you have and questions please call the <?php echo $order['location']['name']; ?> store at <?php echo $order['location']['phone_number']; ?>.<br />
    <br />
    <br />
	Sincerely,<br />
    <br />
	Christophe’s Foodies
</p>

<?php echo $this->load->view('email_templates/includes/order_summary', array('order' => $order), TRUE); ?>

<?php echo $this->load->view('email_templates/includes/footer', FALSE, TRUE); ?>