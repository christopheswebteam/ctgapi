<?php echo $this->load->view('email_templates/includes/header', FALSE, TRUE); ?>

<table bgcolor="#99FF99" style="padding:20px;margin:20px 0px;width:400px;">
    <tr>
        <td>
            <b>Uh Oh – There’s a problem! Your order WAS NOT confirmed!</b>
        </td>
    </tr>
</table>

<p>
	Hello <b><?php echo $order['customer']['name_first']; ?></b>. We are sorry to report that we could not complete your online order. Unfortunately there was a problem processing the credit card you provided to us and we could not charge it. We have cancelled your order and your credit card has not been charged.<br />
	<br />
	Mistakes happen, so if you feel that the problem with your credit card charge was an error you can attempt another order online. Or you may call the <?php echo $order['location']['name']; ?> store directly at <?php echo $order['location']['phone_number']; ?>. Our Foodies can take your order over the phone.<br />
	<br />
	Sincerely,<br />
	<br />
	Christophe’s Foodies
</p>

<?php echo $this->load->view('email_templates/includes/order_summary', array('order' => $order), TRUE); ?>

<?php echo $this->load->view('email_templates/includes/footer', FALSE, TRUE); ?>