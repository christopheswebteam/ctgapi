<?php echo $this->load->view('email_templates/includes/header', FALSE, TRUE); ?>

<table bgcolor="#99FF99" style="padding:20px;margin:20px 0px;width:400px;">
    <tr>
        <td>
            <b>Your order has been received!</b>
        </td>
    </tr>
</table>

<p>
	Hello <b><?php echo $order['customer']['name_first']; ?></b>. We have received your online order to pick up Christophe’s delicious food at the <?php echo $order['location']['name']; ?> store for pickup tomorrow. Unfortunately we received your order after 12:00 pm cut off time for our kitchen for tomorrow’s pickup orders, but we will still do our best to fulfill your order.  You should expect to receive an email update concerning your order before 11:00 am tomorrow morning.  When the order is ready for pick up you will receive a confirmation email.  Please check your email for updates to your order status.  Your credit card will not be charged until your order has been confirmed.<br />
	<br />
	The details of your order are below. Please check the order. If you made a mistake please call the <?php echo $order['location']['name']; ?> store immediately at <?php echo $order['location']['phone_number']; ?>. Orders cannot be cancelled once they are confirmed.<br />
	<br />
	Thank you for your order. We will be in touch soon. Please make sure to check your junk folder to ensure you receive our order status updates.<br />
	<br />
	Sincerely,<br />
	<br />
	Christophe’s Foodies
</p>

<?php echo $this->load->view('email_templates/includes/order_summary', array('order' => $order), TRUE); ?>

<?php echo $this->load->view('email_templates/includes/footer', FALSE, TRUE); ?>