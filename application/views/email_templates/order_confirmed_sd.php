<?php echo $this->load->view('email_templates/includes/header', FALSE, TRUE); ?>

<table bgcolor="#99FF99" style="padding:20px;margin:20px 0px;width:400px;">
    <tr>
        <td>
            <b>Your order has been received!</b>
        </td>
    </tr>
</table>

<p>
	Hello <b><?php echo $order['customer']['name_first']; ?></b>. We are happy to report that your online order has been confirmed and your credit card has been charged. Your order is ready for pick up at the <?php echo $order['location']['name']; ?> The store is open today until <?php echo $order['location']['closing_time']; ?>.  Simply give the store Foodie your name and we will promptly retrieve your order. You can reach the store at <?php echo $order['location']['phone_number']; ?>.<br />
	<br />
	Thank you for your order.<br />
	<br />
	Sincerely,<br />
	<br />
	Christophe’s Foodies
</p>

<?php echo $this->load->view('email_templates/includes/order_summary', array('order' => $order), TRUE); ?>

<?php echo $this->load->view('email_templates/includes/footer', FALSE, TRUE); ?>