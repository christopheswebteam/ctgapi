<?php echo $this->load->view('email_templates/includes/header', FALSE, TRUE); ?>

<table bgcolor="#99FF99" style="padding:20px;margin:20px 0px;width:400px;">
    <tr>
        <td>
            <b>Uh Oh – There’s a problem! Your order WAS NOT confirmed!</b>
        </td>
    </tr>
</table>

<p>
	Hello <b><?php echo $order['customer']['name_first']; ?></b>. We are sorry to report that we could not complete your online order. Unfortunately we ran out of some of the items you ordered before we could complete your order. We have cancelled your order and your credit card has not been charged.<br />
	<br />
	If you would like to order something else you may do so on our website or you may call the <?php echo $order['location']['name']; ?> store directly at <?php echo $order['location']['phone_number']; ?>.  Our Foodies can take your order over the phone, and they will make sure that the items you want are in stock before completing your order.<br />
	<br />
	We are sorry we ran out before we could confirm your order.<br />
	<br />
	Sincerely,<br />
	<br />
	Christophe’s Foodies
</p>

<?php echo $this->load->view('email_templates/includes/order_summary', array('order' => $order), TRUE); ?>

<?php echo $this->load->view('email_templates/includes/footer', FALSE, TRUE); ?>