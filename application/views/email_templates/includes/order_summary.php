<table style="padding:2px;box-sizing:border-box;margin-top:15px;">

	<tr>
		<td style="background-color:#000000;padding:10px;color:#FFFFFF;font-weight:bold;box-sizing:border-box;width:200px;">Item</td>
		<td style="background-color:#000000;padding:10px;color:#FFFFFF;font-weight:bold;box-sizing:border-box;width:70px;">Quantity</td>
		<td style="background-color:#000000;padding:10px;color:#FFFFFF;font-weight:bold;box-sizing:border-box;width:70px;">Cost</td>
	</tr>

	<?php if ((isset($order['products'])) && (is_array($order['products']))): ?>
	
		<?php foreach ($order['products'] as $product): ?>
		
			<tr>
				<td style="background-color:#F6F6F6;padding:10px;color:#FFFFFF;color:black;box-sizing:border-box;">
					<?php echo $product['name']; ?>
				</td>
				<td style="background-color:#F6F6F6;padding:10px;color:#FFFFFF;color:black;box-sizing:border-box;text-align:center;">
					<?php echo $product['quantity']; ?>
				</td>
				<td style="background-color:#F6F6F6;padding:10px;color:#FFFFFF;color:black;box-sizing:border-box;">
					<?php echo $product['total_price']; ?>
				</td>
			</tr>
			
		<?php endforeach; ?>
		
	<?php endif; ?>

	<tr>
		<td style="background-color:#FFFFFF;"></td>
		<td style="background-color:#000000;padding:10px;color:#FFFFFF;font-weight:bold;box-sizing:border-box;width:70px;">Tax</td>
		<td style="background-color:#F6F6F6;padding:10px;color:#FFFFFF;color:black;box-sizing:border-box;">
			<?php echo $order['tax']; ?>
		</td>
	</tr>
	<tr>
		<td style="background-color:#FFFFFF;"></td>
		<td style="background-color:#000000;padding:10px;color:#FFFFFF;font-weight:bold;box-sizing:border-box;width:70px;">Total</td>
		<td style="background-color:#F6F6F6;padding:10px;color:#FFFFFF;color:black;box-sizing:border-box;">
			<?php echo $order['total']; ?>
		</td>
	</tr>

</table>